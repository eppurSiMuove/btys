# btys

### bash-based terminal youtube searching
`btys` - pronouced as _bytes_ - is a simple bash script for youtube searching inside the terminal, it parses the first searching result page seeking for videos metadata, and store these data in variables so it can show you them in a list.

### Requirements
You need to have installed in your system the following packages so this script can work:
- `bash`
- `yt-dlp`
- `mpv`

### Usage
```shell
$ ./btys.sh search-query [type-of-search]

    Where:

        - search-query: a quoted string with terms you want to search for
        - type-of-search: the type of search you want to do in youtube. It can be one of these below:
            - short-video: searches for short videos (less than 4 minutes).
            - medium-video: searches for medium videos (between 4 and 20 minutes). This is the default.
            - long-video: searches for long videos (greater than 20 minutes).
            - channel: searches for channels. # Not working yet
            - playlist: searches for playlist. # Not working yet
    
    Exemple:

    ./btys.sh "shellscript mapfile bash" "long-video"
    [0]. Shell Scripting - Comando read - Ler dados do teclad...
	    Bóson Treinamentos	há 9 anos	20:37

    [1]. Shell Scripting - Criando caixas de diálogo TUI com ...
	    Bóson Treinamentos	há 7 anos	22:08

    [2]. Criação e automação de BACKUPS com Shell Script - Linux
	    Slackjeff	há 1 ano	34:36

    [3]. Curso Básico de Programação em Bash - 03 - Nosso pri...
	    debxp	há 3 anos	56:46

        .
        .
        .

    [16]. Environmental Variables , File System , Booting Proc...
	    Eghost Deusto	Transmitido há 7 anos	1:55:15

    [17]. CSE224B 20200925
	    Dr. Meenu Chopra	há 2 anos	54:14

     > Type the [n]umber of the video you want to watch:

```
![This is how `btys` looks like...](https://gitlab.com/eppurSiMuove/btys/-/raw/main/tests/20230403-012653.avi.gif)
