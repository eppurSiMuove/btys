#!/usr/bin/env bash
# -----------------------------------------------------
# Nome		: btys.sh
# Descrição	: bash-based terminal youtube searching - btys.sh
# Versão	: 0.1-beta
# Autor		: Eppur Si Muove
# Contato	: eppur.si.muove@keemail.me
# Criação	: 24/03/2023
# Modificação:
# Licença	: GNU/GPL v3.0
# CDNEditor	: vim
# -----------------------------------------------------
declare WgetLog='/tmp/btysWgetLog.tmp'
declare PageResult='/tmp/btysPageResult.tmp'
declare ResultByLine='/tmp/btysResultByLine.tmp'
declare ChannelVideosByLine='/tmp/btysChannelVideosByLine.tmp'
declare BaseURL='https://www.youtube.com/'
declare VideoURL=${BaseURL}'watch?v='
declare -a VidID
declare -a VidTitle
declare -a VidChannel
declare -a VidDuration
declare -a VidReleaseDate
declare -a ChannelURL
declare -a ChannelName
declare -a ChannelDescription
declare -a ChannelSubscribersCount
declare -A Filter=(             \
	[short-video]="EgQQARgB"      \
	[medium-video]="EgQQARgD"     \
	[long-video]="EgQQARgC"       \
	[channel]="EgIQAg%253D%253D"  \
	[playlist]="EgIQAw%253D%253D" \
)
declare ArgQuery=${1:-'debxp'}
declare ArgFilter=${2:-'medium-video'}

function _searchResult(){
	local query="${1// /+}"
	local filter=${Filter[$2]}
	local url=${BaseURL}"results?search_query=${query}&sp=${filter}"
	wget -q "$url" -O $PageResult -o $WgetLog
}

function _foundChannels(){
	grep -Po "$regex" $PageResult > $ResultByLine
}

function _foundItems(){
	local videoSearch='searchVideoResultEntityKey":"[^"]+"}},{"videoRenderer.*?(}}}}},)'
	local channelSearch='channelRenderer":{"channelId":"[^"]+",.*?(}}}]}}})'
	local playlistSearch='playlistRenderer":{"playlistId":"[^"]+",.*?(}]}}}]}})'
	local regex=

	if [[ "$1" = *-video ]]; then
		regex="$videoSearch"
	elif [[ "$1" = channel ]]; then
		regex="$channelSearch"
	elif [[ "$1" = playlist ]]; then
		regex="$playlistSearch"
	fi

	grep -Po "$regex" $PageResult > $ResultByLine
}

function _getVideoID(){
	local reVidId='s/^.*\{"videoRenderer":\{"videoId":"([^"]+)".*$/\1/p'
	mapfile -t VidID < <(sed -En "$reVidId" $ResultByLine)
}

function _getVideoTitle(){
	local reVidTitle='s/^.*"title":\{"runs":\[\{"text":"([^"]+)"\}\].*$/\1/p'
	mapfile -t VidTitle < <(sed -En "$reVidTitle" $ResultByLine)
}

function _getVideoChannel(){
	local reVidChannel='s/^.*longBylineText":\{"runs":\[\{"text":"([^"]+)".*$/\1/p'
	mapfile -t VidChannel < <(sed -En "$reVidChannel" $ResultByLine)
}

function _getVideoDuration(){
	local reVidDuration='s/^.*lengthText.*"\}\},"simpleText":"([^"]+)".*$/\1/p'
	mapfile -t VidDuration < <(sed -En "$reVidDuration" $ResultByLine)
}

function _getVideoReleaseDate(){
	local reVidReleaseDate='s/^.*publishedTimeText":\{"simpleText":"([^"]+)".*$/\1/p'
	mapfile -t VidReleaseDate < <(sed -En "$reVidReleaseDate" $ResultByLine)
}

function _getChannelURL(){
	local reChannelURL='s/^.*canonicalBaseUrl":"\/(@[^"]*)"}},.*$/\1/p'
	mapfile -t ChannelURL < <(sed -En "$reChannelURL" $ResultByLine)
}

function _getChannelName(){
	local reChannelName='s/^.*title":\{"simpleText":"([^"]*)".*$/\1/p'
	mapfile -t ChannelName < <(sed -En "$reChannelName" $ResultByLine)
}

function _getChannelDescription(){
	local reChannelDescription='s/^.*descriptionSnippet":\{"runs":\[\{"text":"([^"]*)".*$/\1/p'
	mapfile -t ChannelDescription < <(sed -En "$reChannelDescription" $ResultByLine)
}

function _getChannelSubscribersCount(){
	local reChannelSubscribers='s/^.*videoCountText.*label":"([^"]*)".*$/\1/p'
	mapfile -t ChannelSubscribersCount < <(sed -En "$reChannelSubscribers" $ResultByLine)
}

function _extratcVideoData(){
	_getVideoID
	_getVideoTitle
	_getVideoChannel
	_getVideoDuration
	_getVideoReleaseDate
}

function _showVideoResults(){
	for (( i=0; i <= ${#VidID[@]} - 1; i++ )); do
		[[ ${#VidTitle[i]} -gt 55 ]] && VidTitle[i]="${VidTitle[i]::52}..."
		[[ ${#VidChannel[i]} -gt 25 ]] && VidChannel[i]="${VidChannel[i]::22}..."
		echo "[$i]. ${VidTitle[i]}"
		echo -e "\t${VidChannel[i]}\t${VidReleaseDate[i]}\t${VidDuration[i]}\n"
	done
}

function _extractChannelData(){
	_getChannelURL
	_getChannelName
	_getChannelDescription
	_getChannelSubscribersCount
}

function _showChannelResults(){
	for (( i=0; i <= ${#ChannelURL[@]} - 1; i++ )); do
		echo -en "[$i]. ${ChannelName[i]}\t${ChannelSubscribersCount[i]}\n"
		[[ ${#ChannelDescription[i]} -gt 60 ]] && ChannelDescription[i]="${ChannelDescription[i]::55}..."
		echo -e "${ChannelDescription[i]}\n"
	done
}

function _getChannelVideosPage(){
	local url=${BaseURL}${1}
}

function _playVideo(){
	read -r -p " > Type the [n]umber of the video you want to watch: "
	local url="${VideoURL}${VidID[REPLY]}"
	mpv --ytdl-format="bestvideo[height<=480]+bestaudio/best" "$url" 
}

# ================= #
# ----- BEGIN ----- #
# ================= #

_searchResult "$ArgQuery" "$ArgFilter"
_foundItems "$ArgFilter"

case $ArgFilter in
	"short-video" | "medium-video" | "long-video")
		_extratcVideoData
		_showVideoResults
		_playVideo
		;;
	"channel")
		_extractChannelData
		_showChannelResults
		;;
	"playlist")
		_showPlaylistResults
		_showVideoResults
		;;
	"*")
		echo "Wrong Filter parameter." & exit 1
		;;
esac
